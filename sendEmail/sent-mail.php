<!--
if you want, uncomment that code and it will redirect the users automatically back to contact.php after 4 seconds
<meta http-equiv="refresh" content="4;url=contact.php/">
-->


<?php
// $to is the e-mail to which will be sent the information from the form, so just write your email
$to = "your@mail.here";

/* 
use $_POST to get the values from 
the form and turn them into php variables. 
Those variables will be used for the 
mail() function
*/

$message = $_POST['message'];
$name = $_POST['firstName'];
$lastName = $_POST['lastName'];
$department = $_POST['departement'];
$topic = $_POST['topic'];
$subject =  $department. ": " . $topic;
$from = $_POST['mail'];
$headers = "From:" . $name. " " . $lastName . " <" . $from . ">";

/* 
mail() is a build-in php function
In it you, type the variables that were specified up
*/

mail($to,$subject,$message,$headers);

// echo something to make the user sure, his/her e-mail was sent
echo "Mail Sent.";
?> 

<a href="contact.php" >Go back</a>

<h3>Thanks for your message! You will receive reply from us in 24 hours.</h3>